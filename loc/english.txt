{
    "menu_health_effect_customizer" : "Health Effect Customizer",
	"menu_health_effect_customizer_desc" : "",

	"menu_health_effect_customizer_enable" : "Enable",
	"menu_health_effect_customizer_enable_desc" : "Enable or disable the mod.\nCheck to enable.",

	"menu_health_effect_customizer_transparency" : "Effect Multiplier",
	"menu_health_effect_customizer_transparency_desc" : "The value is a multiple number.0.5 means half the effect.\nRestart to apply."
}
