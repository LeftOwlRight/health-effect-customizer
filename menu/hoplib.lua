
HealthEffectCustomizer = {}

HealthEffectCustomizer.settings = {
	enable = true,
	transparency = 1
}

HealthEffectCustomizer.values = {
	enable = {
        priority = 100
    },
    transparency = {
        min = 0,
        max = 1,
        step = 0.01,
        priority = 5
    }
}


local builder = MenuBuilder:new("health_effect_customizer", HealthEffectCustomizer.settings, HealthEffectCustomizer.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusHealthEffectCustomizer", function(menu_manager, nodes)
  builder:create_menu(nodes)
end)

--localization
dofile(ModPath .. "menu/localization.lua")

