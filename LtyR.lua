if not HealthEffectCustomizer then
	dofile(ModPath.."Core.lua")
end


local multiplier = HealthEffectCustomizer.settings.transparency

function CoreEnvironmentControllerManager:set_health_effect_value(health_effect_value)
	self._health_effect_value = health_effect_value
	if HealthEffectCustomizer.settings.enable then
		self._health_effect_value = health_effect_value * multiplier
	end
end
